.. AdPro - Group 16 documentation master file, created by
   sphinx-quickstart on Thu Mar 17 13:08:37 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AdPro - Group 16's documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
