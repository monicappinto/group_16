# Project Ergo

## About the participants of the Hackathon

We are three Data Scientists participating on a two-day hackathon promoted to study the energy mix of several countries.

Our names and contacts are:

- Carlos Lourenço - 39424@novasbe.pt
- Carolina Cotrim - 39319@novasbe.pt
- Mónica Pinto - 39349@novasbe.pt

## About the project

With the development of this analysis we aim to contribute to the green transition of our company by having a more savvy taskforce. 

We developed some features that we found useful to achieve such goal:
- Gapminder tool to help visualize the GDP, total consumption and population of all countries for a given year
- Evolution of energy's mix consumption between three main countries: Germany, Japan and Canada (these can be altered to whatever the user prefers)
- GDP, total consumption and total emissions' evolution for the same countries (these can be altered to whatever the user prefers)
- Forecast total consumption and emissions for a given country
- Relationship between consumption and emissions from 2010 onwards

## How to access the project

To be able to see our thorough analysis, the only file that needs to be observed is the showcase notebook on the main directory. 
Every method is concentrated on the energy.py file under the functions' directory and is executed on the showcase notebook.