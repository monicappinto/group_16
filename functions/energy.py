import requests
import os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from statsmodels.tsa.arima.model import ARIMA

class Energy:
    """
       Class Energy helps access the database provided via url and allows for an
       in-depth analysis of characteristics such as GDP and energy consumption.
       ...

       Attributes
       ----------
       self.energy_df : pandas.core.frame.DataFrame
           a pandas dataframe with the data provided via url

       Methods
       -------
       download_data()
           Downloads data from an url provided by the professor in class

       countries()
           Outputs a list of available countries in the dataset

       plot_area_chart(country, normalize)
           Builds an area chart of the different consumption columns for a given country
           Normalize option provides such graph in relative terms

       consumption_comparision(countries)
           If only one country is provided, builds a line and dashed-line graph with its total consumption
           and total emissions respectively over the years
           If more countries are provided, builds a line graph and dashed-line with the total consumption
           and total emissions respectively of each over the years so a comparision can be made

       gdp_comparision(countries)
           If only one country provided, builds a line graph with its GDP over the years
           If more countries are provided, builds a line graph with the GDP of each over the years
           so a comparision can be made

       gapminder(year)
           Plots a scatter plot for the given year, where x is GDP, y is total consumption and
           size equals population

       prediction(country, periods)
           Plots two graphs side by side, one with total consumption and another with total emissions
           Both graphs have the past information of the dataset and a forecast based on the
           number of periods chosen by the user

       emissions_consumptions()
           Plots a scatter plot between consumption and emissions for all countries to help understand
           the relationship between both variables
           The countries' population is represented as the size of the dots

    """
    def __init__(self):
        """
        Initializes the class Energy with the attribute energy_df that will be used for its methods
        """
        self.energy_df = pd.DataFrame

    def download_data(self):
        """
        This method downloads data from an url provided by the professor in class and initiates
        the dataframe that we will be working with
        On that, adds all the relevant columns for the study of emissions
        """
        try:
            if not os.path.isfile('../downloads'):
                os.mkdir('../downloads')
        except:
            if os.path.isfile('../downloads/'):
                raise FileExistsError("The folder already exists!")
        finally:
            try:
                if not os.path.isfile('../downloads/energy-data.csv'):
                    filepath = os.path.join("../downloads", "energy-data.csv")
                    r = requests.get("https://nyc3.digitaloceanspaces.com/owid-public/data/energy/owid-energy-data.csv", allow_redirects=True)
                    open(filepath, "wb").write(r.content)
            except:
                if os.path.isfile('../downloads/energy-data.csv'):
                    raise FileExistsError("The file was already downloaded!")
            finally:
                self.energy_df = pd.read_csv("../downloads/energy-data.csv")
                self.energy_df = self.energy_df[self.energy_df["year"] >= 1970]
                self.energy_df.set_index("year", inplace = True)
                self.energy_df.index = pd.to_datetime(self.energy_df.index, format="%Y")

                columns = ["biofuel_consumption", "coal_consumption",
                           "gas_consumption", "hydro_consumption", "nuclear_consumption",
                           "oil_consumption", "solar_consumption", "wind_consumption"]

                emission_values = [1450, 1000, 455, 90, 5.5, 1200, 53, 14]

                for index, column in enumerate(columns):
                    emissions = []
                    for row in self.energy_df[column].items():
                        emissions.append(((row[1] * 1*10**9)/(1*10**6)) * emission_values[index])

                    self.energy_df[f"{column}_emissions"] = emissions

                new_columns = ["biofuel_consumption_emissions", "coal_consumption_emissions",
                           "gas_consumption_emissions", "hydro_consumption_emissions", "nuclear_consumption_emissions",
                           "oil_consumption_emissions", "solar_consumption_emissions", "wind_consumption_emissions"]

                self.energy_df["emissions"] = self.energy_df[new_columns].sum(axis=1)

    def countries(self):
        """
        Outputs a list of available countries in the dataset
        """
        return list(self.energy_df["country"].unique())

    def plot_area_chart(self, country, normalize):
        """
        Builds an area chart of the different consumption columns for a given country
        Normalize option provides such graph in relative terms
        :param country: receives a country to plot the area chart
        :param normalize: if True, provides area chart in relative terms
        :return: area chart
        """

        columns = ["year", "biofuel_consumption", "coal_consumption", "fossil_fuel_consumption", "gas_consumption", "hydro_consumption", "low_carbon_consumption", "nuclear_consumption", "oil_consumption", "other_renewable_consumption", "solar_consumption", "wind_consumption"]
        labels = ["Biofuel", "Coal", "Fossil Fuel", "Gas", "Hydro", "Low Carbon", "Nuclear", "Oil", "Other Renewable", "Solar", "Wind"]
        values = []

        if type(country) not in [str]:
            raise TypeError(f"{country} is not a string.")

        if country in self.countries():
            country_df = self.energy_df[self.energy_df["country"] == country].reset_index()
            country_df = country_df[["year", "biofuel_consumption", "coal_consumption", "fossil_fuel_consumption", "gas_consumption", "hydro_consumption", "low_carbon_consumption", "nuclear_consumption", "oil_consumption", "other_renewable_consumption", "solar_consumption", "wind_consumption"]]
            country_df_dropped = country_df.drop(["year"], axis = 1)

            if normalize == False:
                for column in columns:
                    values.append(country_df[column])

                colors = sns.color_palette("Paired", 13)

                plt.stackplot(values[0], values[1], values[2], values[3], values[4], values[5], values[6], values[7],
                              values[8], values[9], values[10], values[11], labels=labels, colors=colors)

                plt.legend(loc = "upper center", bbox_to_anchor=(1.2, 1), ncol=1, labels = labels)

                plt.title(f"Distribution of Energy on {country}")
                plt.xlabel("Year")
                plt.ylabel("TWh")

                plt.show()

            elif normalize == True:
                normalized_df = country_df_dropped.divide(country_df_dropped.sum(axis=1), axis=0)
                normalized_df.set_index(country_df["year"], inplace = True)
                normalized_df.plot.area(stacked=True, colormap = "Paired")
                plt.legend(loc = "upper center", bbox_to_anchor=(1.2, 1), ncol=1, labels = labels)
                plt.title(f"Distribution of Energy on {country}")
                plt.xlabel("Year")
                plt.ylabel("Percentage")
                plt.show(block=True)
        else:
            raise ValueError(f"{country} is not a valid country in our dataset.")

    def consumption_comparision(self, countries):
        """
        If only one country is provided, builds a line and dashed-line graph with its total consumption
        and total emissions respectively over the years
        If more countries are provided, builds a line graph and dashed-line with the total consumption
        and total emissions respectively of each over the years so a comparision can be made
        :param countries: receives a list of a unique or several countries to build a line graph
        :return: line and dashed-line chart that helps compare the consumption
        """

        if type(countries) not in [list]:
            raise TypeError("Variable countries is not a list.")

        for country in countries:
            if type(country) not in [str]:
                raise TypeError(f"{country} is not a string.")

        fig, ax = plt.subplots()
        ax2 = ax.twinx()

        for country in countries:
            if country in self.countries():
                country_df = self.energy_df[(self.energy_df["country"] == country) & (self.energy_df.index.year < 2020)].reset_index()
                country_df_copy = country_df.copy()
                columns = ["biofuel_consumption", "coal_consumption", "fossil_fuel_consumption", "gas_consumption",
                           "hydro_consumption", "low_carbon_consumption", "nuclear_consumption", "oil_consumption",
                           "other_renewable_consumption", "solar_consumption", "wind_consumption"]

                country_df_copy["summed_consumption"] = country_df_copy[columns].sum(axis=1)

                ax.plot(country_df_copy["year"], country_df_copy["summed_consumption"], label = f"Consumption in {country}")
                ax2.plot(country_df_copy["year"], country_df_copy["emissions"], label = f"Emissions in {country}", linestyle = "--")
                ax.set_xlabel("Year")
                ax.set_ylabel("TWh")
                ax2.set_ylabel("C02 tonnes")

            else:
                raise ValueError(f"{country} is not a valid country in our dataset.")

        fig.legend(loc="upper center", bbox_to_anchor=(1.2, 0.9), ncol=1)
        plt.title("Total Energy Consumption and Emissions evolution")
        plt.show()

    def gdp_comparision(self, countries):
        """
        If only one country is provided, builds a line graph with its GDP over the years
        If more countries are provided, builds a line graph with the GDP of each over the years
        so a comparision can be made
        :param countries: receives a list of a unique or several countries to build a line graph
        :return: line chart that helps compare GDP
        """

        if type(countries) not in [list]:
            raise TypeError("Variable countries is not a list.")

        for country in countries:
            if type(country) not in [str]:
                raise TypeError(f"{country} is not a string.")

        fig, ax = plt.subplots()
        for country in countries:
            if country in self.countries():
                country_df = self.energy_df[(self.energy_df["country"] == country)].reset_index()
                country_df.plot(ax = ax, x = "year", y="gdp", label=country)
            else:
                raise ValueError(f"{country} is not a valid country in our dataset.")

        plt.ylabel("Trillion dollars")
        plt.title("GDP evolution")
        plt.xlabel("Year")
        plt.show()

    def gapminder(self, year):
        """
        Plots a scatter plot for the given year, where x is GDP, y is total consumption and
        size equals population
        :param year: receives a year to plot the scatter plot
        :return: scatter plot for the given year
        """

        if type(year) not in [int]:
            raise TypeError("Variable year is not an integer.")

        years = self.energy_df.index.year
        years_available = pd.unique(years)

        if year in years_available:
            year_df = self.energy_df[years == year]
            columns = ["biofuel_consumption", "coal_consumption", "fossil_fuel_consumption", "gas_consumption",
                       "hydro_consumption", "low_carbon_consumption", "nuclear_consumption", "oil_consumption",
                       "other_renewable_consumption", "solar_consumption", "wind_consumption"]

            year_df_copy = year_df.copy()
            year_df_copy["summed_consumption"] = year_df_copy[columns].sum(axis=1)
            year_df_copy["population_1M"] = year_df_copy["population"]/1000000
            year_df_copy.plot.scatter(x = "gdp", y = "summed_consumption", s="population_1M", alpha = 0.6)

        else:
            raise ValueError(f"{year} is not a valid year in our dataset.")

        plt.xscale("log")
        plt.yscale("log")
        plt.xlabel("GDP in trillion dollars")
        plt.ylabel("Total Consumption in TWh")
        plt.title(f"GDP vs Total Consumption vs Population in {year}")
        plt.show()

    def prediction(self, country, periods):
        """
        Plots two graphs side by side, one with total consumption and another with total emissions
        Both graphs have the past information of the dataset and a forecast based on the
        number of periods chosen by the user
        :param country: receives a country to plot both charts
        :param periods: receives the number of periods the forecast should be done to
        :return: two graphs side by side with the predicted consumption and predicted emissions
        """
        if len(str(periods)) < 1:
            raise Exception("There must be at least one period.")

        if type(periods) not in [int]:
            raise Exception("Variable periods is not an integer.")

        #self.energy_df.index = self.energy_df.index.year
        country_df = self.energy_df[(self.energy_df["country"] == country)].reset_index()

        columns = ["biofuel_consumption", "coal_consumption", "fossil_fuel_consumption", "gas_consumption",
                   "hydro_consumption", "low_carbon_consumption", "nuclear_consumption", "oil_consumption",
                   "other_renewable_consumption", "solar_consumption", "wind_consumption"]

        country_df["summed_consumption"] = country_df[columns].sum(axis=1)
        country_df.set_index("year", inplace=True)
        country_df.index = country_df.index.year
        country_df = country_df[country_df.index <= 2019]
        country_df = country_df[["summed_consumption", "emissions"]]
        country_df.index = pd.to_datetime(country_df.index, format="%Y")
        country_df.index = pd.DatetimeIndex(country_df.index.values, freq=country_df.index.inferred_freq)

        model_consumption = ARIMA(country_df["summed_consumption"], order=(5, 1, 1))
        model_fit_consumption = model_consumption.fit()

        predict_consumption = model_fit_consumption.predict(len(country_df["summed_consumption"]), len(country_df["summed_consumption"]) + periods - 1, typ='levels')
        df_consumption = pd.Series.to_frame(predict_consumption)

        fig, axes = plt.subplots(nrows = 1, ncols = 2, figsize=(12, 5))
        axes[0].plot(country_df.index, country_df["summed_consumption"])
        axes[0].plot(df_consumption.index, df_consumption["predicted_mean"], c = "r")
        axes[0].set_xlabel("Year")
        axes[0].set_ylabel("TWh")
        axes[0].set_title(f"Forecast of Total Consumption for {country} for {periods} years", pad = 20)


        model_emissions = ARIMA(country_df["emissions"], order=(5, 1, 1))
        model_fit_emissions = model_emissions.fit()

        predict_emissions = model_fit_emissions.predict(len(country_df["emissions"]),
                                 len(country_df["emissions"]) + periods - 1, typ='levels')
        df_emissions = pd.Series.to_frame(predict_emissions)

        axes[1].plot(country_df.index, country_df["emissions"])
        axes[1].plot(df_emissions.index, df_emissions["predicted_mean"], c = "r")
        axes[1].set_xlabel("Year")
        axes[1].set_ylabel("C02 Tonnes")
        axes[1].set_title(f"Forecast of Total Emission for {country} for {periods} years", pad = 20)
        plt.show()

    def emissions_consumptions(self):
        """
        Plots a scatter plot between consumption and emissions for all countries to help understand
        the relationship between both variables
        The countries' population is represented as the size of the dots
        :return: scatter plot between consumption and emissions for all countries
        """
        columns = ["biofuel_consumption", "coal_consumption", "fossil_fuel_consumption", "gas_consumption",
                   "hydro_consumption", "low_carbon_consumption", "nuclear_consumption", "oil_consumption",
                   "other_renewable_consumption", "solar_consumption", "wind_consumption"]

        years = self.energy_df.index.year
        year_df = self.energy_df[years >= 2010]

        year_df_copy = year_df.copy()
        year_df_copy["summed_consumption"] = year_df_copy[columns].sum(axis=1)
        year_df_copy["population_1M"] = year_df_copy["population"] / 1000000
        year_df_copy.plot.scatter(x="emissions", y="summed_consumption", s="population_1M", alpha=0.1)
        plt.xscale("log")
        plt.yscale("log")
        plt.xlabel("Total Emissions in C02 Tonnes")
        plt.ylabel("Total Consumption in TWh")
        plt.title(f"Total Emissions vs Total Consumption vs Population")
        plt.show()
